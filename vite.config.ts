import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { VitePWA } from 'vite-plugin-pwa'

const BASE_URL = "/projects/nampoll";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    VitePWA({
      registerType: 'autoUpdate',
      includeAssets: [
        "favicon.svg",
        "favicon.ico",
        "robots.txt",
        "apple-touch-icon.png",
      ],
      manifest: {
        name: "Namibian Live Polling",
        short_name: "NamPoll",
        start_url: ".",
        display: "standalone",
        background_color: "#fff",
        description: "Namibian Live Election voting and polling system",
        icons: [
          {
            src: BASE_URL + "/pwa-192x192.png",
            sizes: "192x192",
            type: "image/png",
          },
          {
            src: BASE_URL + "/pwa-512x512.png",
            sizes: "512x512",
            type: "image/png",
          },
        ],
      },
    }),
  ],
  server: {
    host: "0.0.0.0",
  },
});
