import { createRouter, createWebHistory } from 'vue-router';
import VerifyProfile from './views/VerifyProfile.vue';
import VerifyNVC from './views/VerifyNVC.vue';
import Vote from "./views/Vote.vue";
import VoteConfirm from './views/VoteConfirm.vue';
import VoteComplete from './views/VoteComplete.vue';
import Polls from './views/Polls.vue';
import Home from './views/Home.vue';
import CandidateInfoList from './views/CandidateInfoList.vue';

export default createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    { path: "/", component: Home },
    { path: "/verify/profile", component: VerifyProfile },
    { path: "/verify/nvc", component: VerifyNVC },
    { path: "/vote/cast", component: Vote },
    { path: "/candidates", component: CandidateInfoList },
    { path: "/vote/confirm", component: VoteConfirm },
    { path: "/vote/complete", component: VoteComplete },
    { path: "/polls", component: Polls },
  ],
});
