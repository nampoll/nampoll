import { createApp } from 'vue'
import App from './App.vue'
import Oruga from '@oruga-ui/oruga-next'
import { bulmaConfig } from '@oruga-ui/theme-bulma'
import './style.scss'
import router from './router';
import { createPinia } from 'pinia';
import { registerSW } from "virtual:pwa-register";

registerSW();

createApp(App)
  .use(Oruga, {
    ...bulmaConfig,
    iconPack: "mdi",
  })
  .use(createPinia())
  .use(router)
  .mount("#app");
