import {ref, onMounted} from 'vue';
import useVotingStore from "../stores/voting";
import { useProgrammatic } from "@oruga-ui/oruga-next";
import { storeToRefs } from "pinia";

export function useCandidates() {
  const { oruga } = useProgrammatic();
  const votingStore = useVotingStore();
  const { candidates } = storeToRefs(votingStore);
  const loading = ref(false);

  onMounted(async () => {
    if (votingStore.candidates.length === 0) {
      loading.value = true;
      try {
        await votingStore.fetchCandidates();
      } catch (error: unknown) {
        oruga.notification.open({
          message: String(error),
          duration: 5000,
          position: "top",
          variant: "danger",
          closable: true,
        });
      } finally {
        loading.value = false;
      }
    }
  });

  return { candidates, loading };
}