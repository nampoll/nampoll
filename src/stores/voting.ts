import { RealtimeSubscription } from "@supabase/supabase-js";
import { defineStore } from "pinia";
import supabase from "../api/supabase";
import { isUnknownObject } from "../utils";
import useUserStore from "./user";

export interface Candidate {
  candidate_id: number;
  party_id: number;
  name: string;
  party: string;
  bio: string;
}

export interface Vote {
  party_id: number;
  candidate: {
    id: number;
    name: string;
    party: string;
  };
}

export interface CandidateVotes {
  candidate: Vote['candidate'];
  count: number;
}

export interface PartyVotes {
  party: string;
  party_id: number;
  count: number;
}

interface CandidateResponse {
  candidate_id: number;
  party_id: number;
  parties: {
    party_id: number;
    name: string;
  };
  name: string;
  bio: string;
}

function isCandidateResponse(item: unknown): item is CandidateResponse {
  return (
    isUnknownObject(item) &&
    typeof item.candidate_id === "number" &&
    typeof item.name === "string" &&
    typeof item.bio === "string" && 
    isUnknownObject(item.parties) &&
    typeof item.parties.name === 'string'
  );
}

interface RawVote {
  id: number;
  candidate_id: number;
}


function isVoteResponse(item: unknown): item is RawVote {
  return isUnknownObject(item) && typeof item.id === 'number' && typeof item.candidate_id === 'number';
}

let votesSubscription: RealtimeSubscription | null = null;

const useVotingStore = defineStore("voting", {
  state: () => ({
    candidates: [] as Candidate[],
    selectedCandidateId: null as number | null,
    votedCandidateId: null as number | null,
    votedPartyId: null as number | null,
    totalVotes: 0,
    rawVotes: {} as {[id: number]: RawVote},
  }),
  getters: {
    getCandidateById: (state) => (id: number) =>
      state.candidates.find((c) => c.candidate_id === id),
    selectedCandidate(state) {
      if (typeof state.votedCandidateId === "number") {
        // return this.getCandidateById(state.votedCandidateId);
        return state.candidates.find((c) => c.candidate_id === state.votedCandidateId);
      }
      return undefined;
    },
    votes: (state): Vote[] => {
      const processed: (Vote|null)[] = Object.values(state.rawVotes).map((v) => {
        const candidate = state.candidates.find((c) => c.candidate_id === v.candidate_id);
        if (candidate) {
          return {
            party_id: candidate.party_id,
            candidate: {
              id: candidate.candidate_id,
              name: candidate.name,
              party: candidate.party
            },
          };
        }
        return null;
      })
      return processed.filter((v) => v !== null) as Vote[];
    },
    votesPerCandidate() {
      const counts: { [candidateId: number]: CandidateVotes } = {};
      this.votes.forEach(v => {
        if (!(v.candidate.id in counts)) {
          counts[v.candidate.id] = {
            candidate: v.candidate,
            count: 0,
          };
        }
        counts[v.candidate.id].count++;
      });
      return Object.values(counts);
    },
    votesPerParty() {
      const counts: { [partyId: number]: PartyVotes } = {};
      this.votes.forEach(v => {
        if (!(v.party_id in counts)) {
          counts[v.party_id] = {
            party: v.candidate.party,
            party_id: v.party_id,
            count: 0,
          };
        }
        counts[v.party_id].count++;
      });
      return Object.values(counts);
    }
  },
  actions: {
    async fetchCandidates() {
      const { data, error } = await supabase
        .from("candidates")
        .select("candidate_id, party_id, name, bio, parties (name)");
      if (error) throw error;
      if (data) {
        this.candidates = data.filter(isCandidateResponse).map((c) => ({
          candidate_id: c.candidate_id,
          name: c.name,
          party_id: c.party_id,
          party: c.parties.name,
          bio: c.bio,
        }));
      }
    },
    setSelectedCandidate(candidateId: number) {
      this.selectedCandidateId = candidateId;
    },
    saveVote(candidateId: number) {
      this.votedCandidateId = candidateId;
      const candidate = this.getCandidateById(candidateId);
      if (candidate) {
        this.votedPartyId = candidate.party_id;
      }
    },
    async castVote() {
      const voter = useUserStore();
      const { data, error } = await supabase.from("votes").insert({
        voter_id: voter.id,
        candidate_id: this.votedCandidateId,
      });
      if (error) throw error;
      return data;
    },
    async subscribeToPolls() {
      this.rawVotes = {};
      const {data, error} = await supabase.from('votes').select('*');
      if (error) throw error;
      if (data) {
        const rawVotes = data.filter(isVoteResponse);
        rawVotes.forEach((vote) => {
          this.rawVotes[vote.id] = vote;
          this.totalVotes++;
        });
      }
      return new Promise((resolve) => {
        votesSubscription = supabase
          .from("votes")
          .on("*", (payload) => {
            if (isVoteResponse(payload.new)) {
              if (
                payload.eventType === "INSERT" ||
                payload.eventType === "UPDATE"
              ) {
                this.rawVotes[payload.new.id] = payload.new;
                if (payload.eventType === "INSERT") {
                  this.totalVotes++;
                }
              } else if (payload.eventType === "DELETE") {
                delete this.rawVotes[payload.new.id];
                this.totalVotes--;
              }
            }
          })
          .subscribe(resolve);
      });
    },
    async unsubscribeFromPolls() {
      if (votesSubscription) {
        supabase.removeSubscription(votesSubscription);
      }
    },
  },
});
export default useVotingStore;