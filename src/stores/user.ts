import {defineStore} from 'pinia';
import supabase from '../api/supabase';

export interface UserProfile {
  name: string;
  id: string;
  birthday: number;
}

const useUserStore = defineStore('user', {
  state: () => ({
    name: '',
    id: '',
    birthday: null as null | number,
  }),
  actions: {
    updateProfile(data: UserProfile) {
      this.name = data.name;
      this.id = data.id;
      this.birthday = data.birthday;
    },
    async uploadNvc(file: File) {
        // FIXME: Actually verify image
      return new Promise((resolve, reject) => {
        setTimeout(resolve, 5000);
      });
      // const {data, error} = await supabase.storage
      //   .from("nvc")
      //   .upload(this.id, file, {
      //     cacheControl: "3600",
      //     upsert: true,
      //   });
      // if (error) throw error;
    },
    async registerVoter(file: File) {
      await this.uploadNvc(file);

      const {data, error} = await supabase.from("voters").upsert({
        name: this.name,
        voter_id: this.id,
        birthday: this.birthday,
      });
      console.log(data, error);
      if (error) throw error;
      return data;
    }
  },
});
export default useUserStore;